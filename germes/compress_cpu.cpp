#include "compress_cpu.h"
#include <vector>

void RLE_compress(int* in, int data_len, int* values, int* lenghts, int* values_lenght)
{
	std::vector<int> *lenghts_vector = new std::vector<int>();
	std::vector<int> *values_vector = new std::vector<int>();

	int prev = in[0];
	values_vector->push_back(prev);
	lenghts_vector->push_back(1);
	int k = 0;

	for (int i=1; i < data_len; i++) {
		if (prev == in[i]) {
			(*lenghts_vector)[k]++;
		}
		else {
			values_vector->push_back(in[i]);
			lenghts_vector->push_back(1);

			k++;
			prev = in[i];
		}
	}

	values = values_vector->data();
	(*values_lenght) = (int)values_vector->size();
	lenghts = lenghts_vector->data();
}

void RLE_decompress(int** out, int* out_len, int* values, int* lenghts, int values_lenght)
{
	std::vector<int> *out_vector = new std::vector<int>();
	
	for (int i = 0; i < values_lenght; i++) {
		for (int j = 0; j < lenghts[i]; j++)
		{
			out_vector->push_back(values[i]);
		}
	}

	*out = out_vector->data();
	(*out_len) = (int)out_vector->size();
}
