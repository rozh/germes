#include <iostream>
#include <vector>
#include "LZW.h"
#include <bitset>

using namespace std;

const int PARTITION = 256; 
const int ENCODE_BITS = 16;
const int DICT_SIZE = 1 << ENCODE_BITS;

string dictionary[DICT_SIZE]; 

void loadDict(){
	for (int i = 0; i <= 255; i++) {
		dictionary[i] = (char)i;
	}
	dictionary[32] = " ";
	dictionary[10] = "\n";
}

void clearDict(){
	for (int i = 0; i <= DICT_SIZE; i++) {
		dictionary[i] = "";
	}
}

bool isInDict(string s){
	for (int i = 0; i<DICT_SIZE; i++) {
		if (dictionary[i] == s) {
			return true;
		}
	}
	return false;
}

int getIndex(string s){
	for (int i = 0; i<DICT_SIZE; i++) {
		if (dictionary[i] == s) {
			return i;
		}
	}
	return -1;
}

void lzwCompress(char* input, int input_length, short** output, int* output_length){
	string current, next;
	vector<char> out_buffer;
	int count, offset;

	count = 0;
	offset = 0;

	loadDict();

	while (count<input_length) {
		current = input[count];
		next = input[count + 1];

		if (!isInDict(current + next)) {
			dictionary[PARTITION + offset] = current + next;
			offset++;
			count++;

		}
		else {
			//while (isInDict(current + next)) {
			//	current = current + next;
			//	next = input [count + 2];
			//	count += 1;
			//}
			dictionary[PARTITION + offset] = current + next;
			offset++;
			count++; 

		}

		auto number = (short)getIndex(current);
		if (number<0)
		{
			int a = 0;
		}

		out_buffer.push_back((short)getIndex(current));
	}

	*output = (short*)malloc(sizeof(short)*out_buffer.size());
	
	memcpy(*output, out_buffer.data(), sizeof(short)*out_buffer.size());
	*output_length = out_buffer.size();
	out_buffer.clear();
	clearDict();
}


void lzwDecompress(short* input, int input_length, char** output, int* output_length){
	short current, next;
	int count, offset;
	unsigned long currentNum, nextNum;
	string out_buffer;
	//string *content = new string(input, input_length);
	bitset<ENCODE_BITS> binIndex;

	count = 0;
	offset = 0;

	loadDict();

	while (count<input_length) {

		current = input[count];// content->substr(count, ENCODE_BITS);
		next = input[count+1];  //content->substr(count + ENCODE_BITS, ENCODE_BITS);
		//binIndex = bitset<ENCODE_BITS>(current);
		currentNum = current;// binIndex.to_ullong();

		//binIndex = bitset<ENCODE_BITS>(next);
		nextNum = next;// binIndex.to_ullong();

		if (dictionary[nextNum] == "") {
			dictionary[PARTITION + offset] = dictionary[currentNum] + dictionary[currentNum].substr(0, 1);
		}
		else {
			dictionary[PARTITION + offset] = dictionary[currentNum] + dictionary[nextNum].substr(0, 1);
		}

		dictionary[PARTITION + offset] = dictionary[currentNum] + dictionary[nextNum].substr(0, 1);

		out_buffer.append(dictionary[currentNum]);

		offset++;
		count++;
	}

	*output = (char*)out_buffer.data();
	*output_length = out_buffer.size();
	clearDict();
}

