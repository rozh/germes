#include "FileLoader.h"
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <sstream>

using namespace File;
using namespace germes;

std::vector<ColumnMetaInformation> FileLoader::LoadMetas(std::string filename)
{
	std::ifstream metafile(filename);
	std::string str;
	std::vector<ColumnMetaInformation> metas;

	while (std::getline(metafile, str))
	{

		std::stringstream   linestream(str.data());
		std::string         data;

		data.clear();
		std::getline(linestream, data, '\t');

		ColumnMetaInformation meta;
		meta.Name = (char*)malloc(sizeof(char)*data.length() + 1);
		data.copy(meta.Name, data.length());
		meta.Name[data.length()] = '\0';
		meta.NameLengt = data.length();

		data.clear();
		std::getline(linestream, data, '\t');

		if (data == "INT")
		{
			meta.Type = germes::DataType::INT;
		} else if (data == "FLOAT")
		{
			meta.Type = germes::DataType::FLOAT;
		}
		else if (data == "STRING")
		{
			meta.Type = germes::DataType::STRING;
		}

		linestream >> meta.Size;

		data.clear();
		std::getline(linestream, data, '\t');
		data.clear();
		std::getline(linestream, data, '\n');


		char delim = '|';
		std::vector<std::string> strings;

		auto i = 0;
		auto pos = data.find(delim);
		if (pos != std::string::npos)
		{
			while (pos != std::string::npos) {
				strings.push_back(data.substr(i, pos - i));
				i = ++pos;
				pos = data.find(delim, pos);

				if (pos == std::string::npos)
					strings.push_back(data.substr(i, data.length()));
			}
		}
		else
		{
			strings.push_back(data);
		}

		CompressionChain *chain = nullptr;
		i = 0;
		for (i = strings.size() - 1; i >=0; i--)
		{
			CompressionChain *new_chain = (CompressionChain*)malloc(sizeof(CompressionChain));

			if (strings[i] == "RLE")
				new_chain->Type = RLE;
			if (strings[i] == "NS")
				new_chain->Type = NS;
			if (strings[i] == "DICT")
				new_chain->Type = DICT;
			if (strings[i] == "SCALE")
				new_chain->Type = SCALE;
			new_chain->next = chain;
			chain = new_chain;
		}

		meta.compression_chain = chain;

		metas.push_back(meta);
	}

	return metas;
}

Table* FileLoader::LoadUncompressedTable(std::string tableName, std::string path)
{
	Table *table = new Table();
	std::string metapath(path);
	std::vector<ColumnMetaInformation> metas = LoadMetas(metapath.append(METAFILE_EXT));
	std::ifstream file(path);

	std::vector < Column > *columns = new std::vector < Column >();
	std::vector < ColumnFileData > *columnsData = new std::vector < ColumnFileData >();

	for (int i = 0; i < metas.size(); i++)
	{
		ColumnMetaInformation meta = metas[i];
		Column column;
		ColumnFileData columnData;

		column.Name = meta.Name;
		column.NameLength = meta.NameLengt;
		column.compression_chain = meta.compression_chain;
		column.DataType = meta.Type;
		columns->push_back(column);

		columnData.Type = meta.Type;
		columnsData->push_back(columnData);
	}

	int buffer_lenght = 1024 * 1024 * 4;
	char *buffer = new char[buffer_lenght];
	int readed_size = 0;
	char delimiter = '|';

	char *line = new char[1];
	int last_buf_size = 0;
	uint64 start_time;
	while (file.read(buffer, buffer_lenght))
	{
		start_time = GetTimeMs64();
		int j = 0;
		while (j < buffer_lenght && buffer[j] != EOF)
		{
			if (buffer[j] == '\n') while (j < buffer_lenght && buffer[j] != EOF &&  buffer[j] == '\n') j++;
			int start_pos = j;

			while (j < buffer_lenght && buffer[j] != EOF &&  buffer[j] != '\n') j++;
			int end_pos = j;

			int size = end_pos - start_pos;
			char *buf_line = new char[size + 1];
			memcpy_s(buf_line, size + 1, buffer + start_pos, size + 1);
			buf_line[size] = '\0';

			if (buffer[j] != '\n')
			{
				line = new char[size + 1];
				memcpy_s(line, size + 1, buf_line, size + 1);
				last_buf_size = size;
			}
			else
			{
				char *full_line = new char[size + 1 + last_buf_size];
				if (last_buf_size > 0)
				{
					memcpy_s(full_line, last_buf_size, line, last_buf_size);
					free(line);
				}
				memcpy_s(full_line + last_buf_size, size + 1, buf_line, size + 1);
				full_line[last_buf_size + size] = '\0';
				ParseLine(full_line, last_buf_size + size, delimiter, columnsData);
				free(full_line);
				last_buf_size = 0;
			}
			free(buf_line);
		}
		readed_size += buffer_lenght;
		std::cout << readed_size / 1024 / 1024 << "M" << "\t" << GetTimeMs64() - start_time << " ms" << std::endl;
	}

	buffer_lenght = file.gcount();
	start_time = GetTimeMs64();
	int j = 0;
	while (j < buffer_lenght && buffer[j] != EOF)
	{
		if (buffer[j] == '\n') while (j < buffer_lenght && buffer[j] != EOF &&  buffer[j] == '\n') j++;
		int start_pos = j;

		while (j < buffer_lenght && buffer[j] != EOF &&  buffer[j] != '\n') j++;
		int end_pos = j;

		int size = end_pos - start_pos;
		char *buf_line = new char[size + 1];
		memcpy_s(buf_line, size + 1, buffer + start_pos, size + 1);
		buf_line[size] = '\0';

		if (buffer[j] != '\n')
		{
			line = new char[size + 1];
			memcpy_s(line, size + 1, buf_line, size + 1);
			last_buf_size = size;
		}
		else
		{
			char *full_line = new char[size + 1 + last_buf_size];
			if (last_buf_size > 0)
			{
				memcpy_s(full_line, last_buf_size, line, last_buf_size);
				free(line);
			}
			memcpy_s(full_line + last_buf_size, size + 1, buf_line, size + 1);
			full_line[last_buf_size + size] = '\0';
			ParseLine(full_line, last_buf_size + size, delimiter, columnsData);
			free(full_line);
			last_buf_size = 0;
		}
		free(buf_line);
	}
	readed_size += buffer_lenght;
	std::cout << readed_size / 1024 / 1024 << "M" << "\t" << GetTimeMs64() - start_time << " ms" << std::endl;


	for (int i = 0; i < columns->size(); i++)
	{
		Column *column = &(*columns)[i];
		ColumnFileData *columnData = &(*columnsData)[i];

		switch (column->DataType)
		{
		case DataType::INT:		column->Ints = columnData->Ints.data(); column->DataLength = columnData->Ints.size(); break;
		case DataType::FLOAT:	column->Floats = columnData->Floats.data(); column->DataLength = columnData->Floats.size();  break;
		case DataType::STRING:	column->Strings = columnData->Strings.data(); column->StringLengths = MaxStringLenght(columnData->Strings); column->DataLength = columnData->Strings.size();  break;
		default: break;
		}
	}

	table->Name = (char*)malloc(tableName.size()+1);
	tableName.copy(table->Name, tableName.size());
	table->Name[tableName.size()] = '\0';
	table->Columns = columns->data();
	table->ColumnCount = columns->size();
	table->NameLength = tableName.size();

	return table;
}

CompressedDatabase FileLoader::LoadCompressed()
{
	std::string str;
	std::ifstream in_file;
	CompressedDatabase compressed_database = *new CompressedDatabase();
	in_file.open(_paths->DatabaseDir + GPUDB_EXT);
	std::getline(in_file, str);
	compressed_database.table_count = atoi(str.data());
	compressed_database.Name = (char *)_paths->DatabaseName.data();
	compressed_database.NameLength = _paths->DatabaseName.length();

	CompressedTable* tables = (CompressedTable *)malloc(sizeof(CompressedTable)*compressed_database.table_count);
	for (int i = 0; i < compressed_database.table_count; i++)
	{
		std::getline(in_file, str);
		CompressedTable *table = new CompressedTable();
		table->Name = (char *)malloc(sizeof(char)*str.length()+1);
		str.copy(table->Name, str.length());
		table->Name[str.length()] = '\0';
		table->NameLength = 5;// str.length();

		LoadCompressedTable(table);
		tables[i] = *table;
	}

	compressed_database.compressed_tables = tables;

	in_file.close();

	return compressed_database;
}

void FileLoader::LoadCompressedTable(CompressedTable* table)
{
	std::ifstream in_file;
	in_file.open(_paths->GetTableDir(table->Name) + METAFILE_EXT);

	std::string str;
	std::getline(in_file, str);
	table->column_count = atoi(str.data());
	table->compressed_columns = (CompressedColumn*)malloc(sizeof(CompressedColumn)*table->column_count);
	
	for (int i = 0; i < table->column_count; i++)
	{
		auto column = new CompressedColumn();
		std::getline(in_file, str);
		column->Name = (char *)malloc(sizeof(char)*str.length() + 1);
		str.copy(column->Name, str.length());
		column->Name[str.length()] = '\0';
		column->NameLength = (int)str.length();
		std::getline(in_file, str);
		int type = atoi(str.data());
		std::getline(in_file, str);
		column->block_count = atoi(str.data());

		LoadCompressedColumn(table->Name, column); 
		table->compressed_columns[i] = *column;
	}
	in_file.close();
	
}

void FileLoader::LoadCompressedColumn(std::string table_name, CompressedColumn* column)
{
	std::string path = _paths->GetColumnDir((char *)table_name.data(), column->Name);
	std::ifstream in_file;
	in_file.open(path + METAFILE_EXT);

	std::string str;
	std::getline(in_file, str);
	column->block_count = atoi(str.data());
	column->data_blocks = (CompressedColumnBlock*)malloc(sizeof(CompressedColumnBlock)*column->block_count);
	std::getline(in_file, str);
	column->Type = (germes::DataType)atoi(str.data());
	std::getline(in_file, str);
	column->NameLength = atoi(str.data());
	std::getline(in_file, str);
	column->Name = (char *)malloc(sizeof(char)*str.length()+1);
	str.copy(column->Name, str.length());
	column->Name[str.length()] = '\0';
	column->compression_chain = LoadCompressionChain(&in_file);

	for (int i = 0; i < column->block_count; i++)
	{
		auto block = new CompressedColumnBlock();
		LoadCompressedColumnBlock(table_name, column->Name, block, i);
		column->data_blocks[i] = *block;
	}
}

void FileLoader::LoadCompressedColumnBlock(std::string table_name, std::string col_name, CompressedColumnBlock* column, int block_number)
{
	std::string path = _paths->GetColumnBlockPath((char *)table_name.data(), (char *)col_name.data(), block_number);
	std::ifstream in_file;
	in_file.open(path + GPUDB_EXT, std::ofstream::in | std::ifstream::binary);
	
	std::string str;
	std::getline(in_file, str);
	column->DataLenght = atoi(str.data());
	std::getline(in_file, str);
	byte read_data = atoi(str.data());
	std::getline(in_file, str);
	byte read_custom1 = atoi(str.data());
	std::getline(in_file, str);
	byte read_custom2 = atoi(str.data());
	std::getline(in_file, str);
	byte read_custom3 = atoi(str.data());
	std::getline(in_file, str);
	byte read_custom4 = atoi(str.data());

	if (read_data > 0) {
		column->Data = (char *)malloc(sizeof(char) * column->DataLenght);
		in_file.read(column->Data, column->DataLenght);
	}
	if (read_custom1 > 0) {
		column->Custom1 = (char *)malloc(sizeof(char) * column->DataLenght);
		in_file.read(column->Custom1, column->DataLenght);
	}
	if (read_custom2 > 0) {
		column->Custom2 = (char *)malloc(sizeof(char) * column->DataLenght);
		in_file.read(column->Custom2, column->DataLenght);
	}
	if (read_custom3 > 0) {
		column->Custom3 = (char *)malloc(sizeof(char) * column->DataLenght);
		in_file.read(column->Custom3, column->DataLenght);
	}
	if (read_custom4 > 0) {
		column->Custom4 = (char *)malloc(sizeof(char) * column->DataLenght);
		in_file.read(column->Custom4, column->DataLenght);
	}
}

void FileLoader::SaveCompressedDb(CompressedDatabase db)
{
	boost::filesystem::path dir(_paths->BaseDir);
	if (boost::filesystem::create_directory(dir));

	boost::filesystem::path db_dir(_paths->DatabaseDir);
	if (boost::filesystem::create_directory(db_dir));
	
	if (boost::filesystem::create_directory(db_dir) || boost::filesystem::is_directory(db_dir))
	{
		std::ofstream out_file;
		out_file.open(_paths->DatabaseDir + GPUDB_EXT);
		out_file << db.table_count << std::endl;

		auto tables = db.compressed_tables;

		for (int i = 0; i < db.table_count; i++)
		{
			SaveCompressedTable(tables[i]);

			out_file << tables[i].Name << std::endl;
		}

		out_file.close();
	}
}

void FileLoader::SaveCompressedTable(CompressedTable table)
{
	std::string table_path(_paths->GetTableDir(table.Name));
	boost::filesystem::path table_dir(table_path.c_str());
	if (boost::filesystem::create_directory(table_dir) || boost::filesystem::is_directory(table_dir))
	{
		for (int i = 0; i < table.column_count; i++)
		{
			FileLoader::SaveCompressedColumn(table.Name, table.compressed_columns[i]);
		}
		SaveCompressedTableMeta(table);
	}
}

void FileLoader::SaveCompressedColumn(std::string table_name, CompressedColumn column)
{
	std::string path = _paths->GetColumnDir((char *)table_name.data(), column.Name);
	boost::filesystem::path dir(path.c_str());
	if (boost::filesystem::create_directory(dir) || boost::filesystem::is_directory(dir))
	{
		std::ofstream out_file;

		out_file.open(path + METAFILE_EXT);
		out_file << column.block_count << std::endl;
		out_file << column.Type << std::endl;
		out_file << column.NameLength << std::endl;
		out_file.write(column.Name, column.NameLength);
		out_file << std::endl;
		WriteCompressionChain(&out_file, column.compression_chain);

		out_file.close();

		for (int i = 0; i < column.block_count; i++)
		{
			SaveCompressedColumnBlock(table_name, column.Name, column.data_blocks[i], i);
		}
	}
}

void FileLoader::WriteCompressionChain(std::ofstream* out_file, CompressionChain* chain)
{
	CompressionChain *current_chain = chain;
	while (current_chain != nullptr)
	{
		*out_file << chain->Type;
		current_chain = current_chain->next;
		if (current_chain != nullptr) *out_file << '|';
	}
	*out_file << std::endl;
}

CompressionChain* FileLoader::LoadCompressionChain(std::ifstream* in_file)
{
	CompressionChain *chain = nullptr;
	char delim = '|';
	std::string str;
	std::vector<std::string> strings;
	std::getline(*in_file, str);

	auto i = 0;
	auto pos = str.find(delim);
	if (pos != std::string::npos)
	{
		while (pos != std::string::npos) {
			strings.push_back(str.substr(i, pos - i));
			i = ++pos;
			pos = str.find(delim, pos);

			if (pos == std::string::npos)
				strings.push_back(str.substr(i, str.length()));
		}
	}
	else
	{
		strings.push_back(str);
	}

	i = 0;
	for (i; i < strings.size(); i++)
	{
		CompressionChain *new_chain = (CompressionChain*)malloc(sizeof(CompressionChain));
		new_chain->Type = (germes::CompressionType)atoi(strings[i].data());
		new_chain->next = chain;
		chain = new_chain;
	}

	return chain;
}

void FileLoader::SaveCompressedColumnBlock(std::string table_name, std::string col_name, CompressedColumnBlock column, int bloc_number)
{
	std::string path = _paths->GetColumnBlockPath((char *)table_name.data(), (char *)col_name.data(), bloc_number);
	boost::filesystem::path dir(path.c_str());

	std::ofstream out_file;
	out_file.open(path + GPUDB_EXT, std::ofstream::out | std::ifstream::binary);
	out_file << column.DataLenght << std::endl;
	out_file << (column.Data != nullptr ? TRUE : FALSE) << std::endl;
	out_file << (column.Custom1 != nullptr ? TRUE : FALSE) << std::endl;
	out_file << (column.Custom2 != nullptr ? TRUE : FALSE) << std::endl;
	out_file << (column.Custom3 != nullptr ? TRUE : FALSE) << std::endl;
	out_file << (column.Custom4 != nullptr ? TRUE : FALSE) << std::endl;

	out_file.write(column.Data, column.DataLenght);
	if (column.Custom1 != nullptr) 
		out_file.write(column.Custom1, column.DataLenght);
	if (column.Custom2 != nullptr) 
		out_file.write(column.Custom2, column.DataLenght);
	if (column.Custom3 != nullptr) 
		out_file.write(column.Custom3, column.DataLenght);
	if (column.Custom4 != nullptr) 
		out_file.write(column.Custom4, column.DataLenght);

	out_file.close();
}

void FileLoader::SaveCompressedTableMeta(CompressedTable table)
{
	std::ofstream out_file;
	out_file.open(_paths->GetTableDir(table.Name) + METAFILE_EXT);
	out_file << table.column_count << std::endl;
	for (int i = 0; i < table.column_count; i++)
	{
		auto column = table.compressed_columns[i];

		out_file << column.Name << std::endl << (column.data_blocks == nullptr ? 0 : column.Type) << std::endl << column.block_count << std::endl;
	}
	out_file.close();
}

void FileLoader::ParseLine(char *line, int length, char delimiter, std::vector<ColumnFileData> *columns)
{
	int start_pos = 0;
	int count = 0;
	std::string token;

	for (int i = 0; i < length; i++)
	{
		if (line[i] == delimiter)
		{
			int size = i - start_pos;
			char *buf = &(line[start_pos]);
			line[i] = '\0';
			if (count < columns->size())
			{
				switch ((*columns)[count].Type)
				{
				default: break;
				case DataType::INT:
					(*columns)[count].Ints.push_back(std::stoi(buf));
					break;
				case DataType::FLOAT:
					(*columns)[count].Floats.push_back(std::stof(buf));
					break;
				case STRING:
					(*columns)[count].Strings.push_back(FileLoader::FillString(buf, size));
					break;
				}
			}
			else
			{
				break;
			}

			count++;
			start_pos = i + 1;
		}
	}
}

int FileLoader::MaxStringLenght(std::vector < char* > strings)
{
	int lenght = 0;
	for (int i = 0; i < strings.size(); i++)
	{
		for (int j = 0; j < 100;j++)
		{
			if (strings[i][j]=='\0')
			{
				if (lenght < j) lenght = j;
				break;
			}
		}
	}
	return lenght;
}

char* FileLoader::FillString(char *str, int size)
{
	char *line = (char*)malloc(sizeof(char) * COLUMN_NAME_SIZE);

//#pragma omp parallel for 
	for (int i = 0; i < COLUMN_NAME_SIZE-1; i++)
	{
		line[i] = i < size ? str[i] : '\0';
	}
	return line;
}