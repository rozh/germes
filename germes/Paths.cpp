#include "Paths.h"

using namespace File;

class Paths;

std::string File::Paths::GetTableDir(char* name)
{
	return BaseDir + "\\" + DatabaseName + "\\" + name;
}

std::string File::Paths::GetColumnDir(char* table_name, char* column_name)
{
	return BaseDir + "\\" + DatabaseName + "\\" + table_name + "\\" + column_name;
}

std::string File::Paths::GetColumnBlockPath(char* table_name, char* column_name, int block_number)
{
	char * buf = (char *)malloc(sizeof(char) * 5);
	sprintf(buf, "%04d", block_number);
	return BaseDir + "\\" + DatabaseName + "\\" + table_name + "\\" + column_name + "\\" + "block." + buf;
}
