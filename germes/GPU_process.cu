#include "GPU_proccess.h"
#include <cstdlib>
#include <cstring>

using namespace germes;

/**
��������� � GPU ������ ������ ��������� RLE � ��������� ��

@column_block ���� �������
@return Column ������� �� ������� �� ������ � GPU
*/
CompressedColumnBlock RLE_Decompress(CompressedColumnBlock column_block)
{
	int size = column_block.DataLenght / sizeof(int);
	int datalen;
	int* gpu_uncompressed;
	gcStream_t s;
	
	gc_stream_start(&s);
	int* gpu_compressed = (int *)(column_block.Data);
	int* gpu_compressed_len = (int *)(column_block.Custom1);

	gc_decompress_rle(s, &gpu_uncompressed, &datalen, gpu_compressed, gpu_compressed_len, size);

	gc_stream_wait(&s);
	gc_stream_stop(&s);

	gc_free(gpu_compressed);
	gc_free(gpu_compressed_len);

	column_block.Data = (char*)gpu_uncompressed;
	column_block.DataLenght = datalen*sizeof(int);
	column_block.Custom1 = nullptr;
	
	return column_block;
}

/**
��������� � GPU ������ ������ ��������� NS � ��������� ��

@column_block ���� �������
@return Column ������� �� ������� �� ������ � GPU
*/
CompressedColumnBlock NS_Decompress(CompressedColumnBlock column_block)
{
	int size = column_block.DataLenght / sizeof(char);
	int datalen = 0;
	int* gpu_uncompressed = nullptr;
	gcStream_t s;

	gc_stream_start(&s);
	char* gpu_compressed = (char *)(column_block.Data);
	gc_decompress_ns(s, gpu_uncompressed, datalen, gpu_compressed, size);

	gc_stream_wait(&s);
	gc_stream_stop(&s);

	gc_free(gpu_compressed);

	column_block.Data = (char*)gpu_uncompressed;
	column_block.DataLenght = datalen*sizeof(int);
	column_block.Custom1 = nullptr;

	return column_block;
}

/**
��������� � GPU ������ ������ ��������� SCALE � ��������� ��

@column_block ���� �������
@return Column ������� �� ������� �� ������ � GPU
*/
CompressedColumnBlock SCALE_Decompress(CompressedColumnBlock column_block)
{
	int size = column_block.DataLenght / sizeof(int);
	float* gpu_uncompressed = nullptr;
	gcStream_t s;

	gc_stream_start(&s);
	int* gpu_compressed = (int *)(column_block.Data);
	gc_decompress_scale(s, gpu_uncompressed, size, gpu_compressed);

	gc_stream_wait(&s);
	gc_stream_stop(&s);

	gc_free(gpu_compressed);

	column_block.Data = (char*)gpu_uncompressed;
	column_block.DataLenght = size*sizeof(float);

	return column_block;
}

/**
��������� � GPU ������ ������

@column_block ���� �������
@return CompressedColumnBlock ������ ������� �� ������� �� ������ � GPU
*/
CompressedColumnBlock GPU_LoadCompressedCollumnBlock(CompressedColumnBlock column_block)
{
	gcStream_t s;
	CompressedColumnBlock gpu_column_block;

	gc_stream_start(&s);

	gpu_column_block.DataLenght = column_block.DataLenght;
	gpu_column_block.Data = static_cast<char *>(gc_host2device(s, column_block.Data, column_block.DataLenght));
	if (column_block.Custom1 != nullptr)
		gpu_column_block.Custom1 = static_cast<char *>(gc_host2device(s, column_block.Custom1, column_block.DataLenght));
	if (column_block.Custom2 != nullptr)
		gpu_column_block.Custom2 = static_cast<char *>(gc_host2device(s, column_block.Custom2, column_block.DataLenght));
	if (column_block.Custom3 != nullptr)
		gpu_column_block.Custom3 = static_cast<char *>(gc_host2device(s, column_block.Custom3, column_block.DataLenght));
	if (column_block.Custom4 != nullptr)
		gpu_column_block.Custom4 = static_cast<char *>(gc_host2device(s, column_block.Custom4, column_block.DataLenght));

	gc_stream_wait(&s);
	gc_stream_stop(&s);
	
	return gpu_column_block;
}

CompressedColumnBlock RLE_Compress(CompressedColumnBlock compressed_column)
{
	CompressedColumnBlock column_block;
	int* gpu_uncompressed;
	int* gpu_compressed = NULL;
	int* gpu_compressed_len = NULL;
	int *compressed_len = (int*)malloc(sizeof(int));
	gcStream_t s;
	gc_stream_start(&s);
	gpu_uncompressed = (int *)(gc_host2device(s, compressed_column.Data, compressed_column.DataLenght));
	gc_compress_rle(s, gpu_uncompressed, compressed_column.DataLenght, &gpu_compressed, &gpu_compressed_len, compressed_len);
	column_block.DataLenght = *compressed_len*sizeof(int);
	column_block.Data = (char *)(gc_device2host(s, gpu_compressed, *compressed_len*sizeof(int)));
	column_block.Custom1 = (char *)(gc_device2host(s, gpu_compressed_len, *compressed_len*sizeof(int)));

	gc_free(gpu_uncompressed);
	gc_stream_wait(&s);
	gc_stream_stop(&s);
	gc_free(gpu_compressed);
	gc_free(gpu_compressed_len);

	return column_block;
}

CompressedColumnBlock SCALE_Compress(CompressedColumnBlock compressed_column)
{
	CompressedColumnBlock column_block;
	float* gpu_uncompressed;
	int* gpu_compressed;
	gcStream_t s;

	int len = compressed_column.DataLenght / sizeof(float);
	gc_stream_start(&s);
	gpu_uncompressed = (float *)(gc_host2device(s, compressed_column.Data, len * sizeof(float)));
	gpu_compressed = (int *)(gc_malloc(len * sizeof(int)));
	gc_compress_scale(s, gpu_uncompressed, len, gpu_compressed);

	column_block.DataLenght = len*sizeof(int);
	column_block.Data = (char *)(gc_device2host(s, gpu_compressed, len*sizeof(int)));

	gc_free(gpu_uncompressed);
	gc_stream_wait(&s);
	gc_stream_stop(&s);	
	gc_free(gpu_compressed);
	
	return column_block;
}

CompressedColumnBlock NS_Compress(CompressedColumnBlock compressed_column)
{
	CompressedColumnBlock column_block;
	int* gpu_uncompressed;
	int len = compressed_column.DataLenght / sizeof(int);
	short* gpu_compressed = (short *)(gc_malloc(len * sizeof(short)));;
	gcStream_t s;
	gc_stream_start(&s);
	gpu_uncompressed = (int *)(gc_host2device(s, compressed_column.Data, compressed_column.DataLenght));
	gc_compress_ns2(s, gpu_uncompressed, len, gpu_compressed, len);

	column_block.DataLenght = len*sizeof(short);
	column_block.Data = (char *)(gc_device2host(s, gpu_compressed, len*sizeof(short)));

	gc_free(gpu_uncompressed);
	gc_stream_wait(&s);
	gc_stream_stop(&s);
	gc_free(gpu_compressed);

	return column_block;
}
CompressedColumnBlock DICT_Compress(CompressedColumnBlock compressed_column)
{
	CompressedColumnBlock column_block;
	int* gpu_uncompressed;
	int len = compressed_column.DataLenght / sizeof(int);
	short* gpu_compressed = (short *)(gc_malloc(len * sizeof(short)));;
	gcStream_t s;
	gc_stream_start(&s);
	gpu_uncompressed = (int *)(gc_host2device(s, compressed_column.Data, compressed_column.DataLenght));
	gc_compress_ns2(s, gpu_uncompressed, len, gpu_compressed, len);

	column_block.DataLenght = len*sizeof(short);
	column_block.Data = (char *)(gc_device2host(s, gpu_compressed, len*sizeof(short)));

	gc_free(gpu_uncompressed);
	gc_stream_wait(&s);
	gc_stream_stop(&s);
	gc_free(gpu_compressed);

	return column_block;
}

/**
��������� � GPU ������ ������ ��������� RLE � ��������� ��

@column_block ���� �������
@return Column ������� �� ������� �� ������ � GPU
*/
Column CompressedColumnBlockToColumn(CompressedColumnBlock column_block, DataType type)
{
	Column col;
	col.DataType = type;
	col.Name = "\0";
	col.NameLength = 1;
	col.DataLength = column_block.DataLenght;

	switch (type)
	{
	case INT:
		col.Ints = (int*)column_block.Data; break;
	case FLOAT:
		col.Floats = (float*)column_block.Data; break;
	case STRING:
		col.Strings = (char**)column_block.Data; 
		col.StringLengths = (int)column_block.Custom1;		
		break; 
	default: break;
	}
	
	return col;
}

/**
��������� ���� � GPU � ��������� ���

@column_block ���� �������
@return Column ������� �� ������� �� ������ � GPU
*/
Column GPU_LoadDataBlock(CompressedColumnBlock column_block, CompressionChain* chain, DataType type)
{
	Column col = {};
	CompressionChain *current_chain = chain;

	CompressedColumnBlock gpu_column_block = GPU_LoadCompressedCollumnBlock(column_block);

	while (current_chain != nullptr)
	{
		switch (type)
		{
		case RLE: gpu_column_block = RLE_Decompress(gpu_column_block);
		case NS: gpu_column_block = RLE_Decompress(gpu_column_block);  break;
		case DICT: break;
		case SCALE:gpu_column_block = SCALE_Decompress(gpu_column_block); break;
		default: break;
		}
		current_chain = current_chain->next;
	}

	col = CompressedColumnBlockToColumn(gpu_column_block, type);
	return col;
}

/**
��������� ������� � GPU � ��������� ��

@compressed_table		������ �������
@column_block_number	����� �����
@return Table �� �������� �� GPU
*/
Table GPU_LoadTableBlock(CompressedTable compressed_table, int column_block_number)
{
	Table tab = {};

	if (compressed_table.column_count>0)
	{
		Column *cols = (Column*)malloc(sizeof(Column)*compressed_table.column_count);
		for (int i = 0; i < compressed_table.column_count; i++)
		{
			auto col = compressed_table.compressed_columns[i];
			if (column_block_number < col.block_count)
			{
				cols[i] = GPU_LoadDataBlock(col.data_blocks[column_block_number], col.compression_chain, col.Type);
				cols[i].Name = col.Name;
				cols[i].NameLength = col.NameLength;
			}
		}
		tab.Name = compressed_table.Name;
		tab.NameLength = compressed_table.NameLength;
		tab.ColumnCount = compressed_table.column_count;
		tab.Columns = cols;
	}

	return tab;
}

/**
�������� ������� �� GPU � ������� �� ������

@gpu_data		������ ���� � GPU
@length			����� ������� ���� � GPU
@return ������ ���� � RAM
*/
char *GPU_RetriveIntData(int *gpu_data, int length)
{
	char *data;
	gcStream_t s;

	gc_stream_start(&s);
	data = static_cast<char*>(gc_device2host(s, gpu_data, length));
	gc_stream_wait(&s);
	gc_stream_stop(&s);
	gc_free(gpu_data);

	return data;
}

/**
�������� ������� �� GPU � ������� �� ������

@column		������� � ������� �� GPU
@return Column �� �������� �� � RAM
*/
Column GPU_RetriveColumn(Column column)
{
	char *data = nullptr;

	switch (column.DataType)
	{
	case INT: data = GPU_RetriveIntData(column.Ints, column.DataLength);
		break;
	case FLOAT: break;
	case STRING: break;
	default: break;
	}

	Column col;
	col.DataLength = column.DataLength;
	col.DataType = column.DataType;
	col.Ints = (int *)data;
	col.Name = column.Name;
	col.NameLength = column.NameLength;
	col.compression_chain = column.compression_chain;

	return col;
}

/**
��������� ������� � GPU � ��������� ��

@table		�������� �������
@gpuTable	������� � GPU
@return Table �� �������� �� GPU
*/
Table GPU_RetriveTable(Table table, Table gpuTable)
{

	Table tab = {};
	Column *cols = (Column*)malloc(sizeof(Column)*(gpuTable.ColumnCount + table.ColumnCount));

	int i = 0;
	for (; i < table.ColumnCount; i++)
	{
		auto col = table.Columns[i];
		cols[i] = col;
	}
	
	for (int j = 0; j < gpuTable.ColumnCount; j++)
	{
		auto col = gpuTable.Columns[j];
		cols[i + j] = GPU_RetriveColumn(col);
	}

	tab.Name = gpuTable.Name;
	tab.NameLength = gpuTable.NameLength;
	tab.ColumnCount = gpuTable.ColumnCount;
	tab.Columns = cols;

	return tab;
}

CompressedTable* GPU_CompressTable(germes::Table table)
{
	CompressedTable *cTable = new CompressedTable();

	int block_element_count = CalculateBlockElementCount(table);

	cTable->Name = table.Name;
	cTable->NameLength = table.NameLength;
	cTable->column_count = table.ColumnCount;
	CompressedColumn *columns = (CompressedColumn*)malloc(sizeof(CompressedColumn)*table.ColumnCount);
	Column *col;
	for (int i = 0; i < table.ColumnCount; i++)
	{
		col = &table.Columns[i];
		columns[i].Name = col->Name;
		columns[i].NameLength = col->NameLength;
		columns[i] = *GPU_CompressColumn(*col, block_element_count);
	}
	cTable->compressed_columns = columns;
	
	return cTable;
}

int* GPU_CopyToDevice(int* data, int len)
{
	int size = len * sizeof(int);
	gcStream_t s;

	gc_stream_start(&s);
	int* gpu_compressed = static_cast<int *>(gc_host2device(s, data, size));
	gc_stream_wait(&s);
	gc_stream_stop(&s);

	return gpu_compressed;
}

void GPU_Free(int* data)
{
	gc_free(data);
}

//CompressedColumn* GPU_CompressColumn(Column column, int block_element_count)
//{
//	CompressedColumn* compressed_column = new CompressedColumn();
//
//
//	switch (column.DataType)
//	{
//	case INT: compressed_column = GPU_CompressColumn_INT(block_element_count, column.Ints, column.DataLength, column.compression_chain); break;
//	case FLOAT: compressed_column = GPU_CompressColumn_FLOAT(block_element_count, column.Floats, column.DataLength); break;
//	case STRING: compressed_column = GPU_CompressColumn_STRING(block_element_count, column.Strings, column.DataLength, column.StringLengths); break;
//	default: break;
//	}
//
//	compressed_column->Name = column.Name;
//	compressed_column->NameLength = column.NameLength;
//	
//	return compressed_column;
//}

int CalculateChunkSize(int size)
{
	return CHUNK_SIZE % size == 0 ? CHUNK_SIZE : size * (CHUNK_SIZE / size + 1);
}

int CalculateBlockCount(int data_length, int block_element_count)
{
	return data_length / block_element_count + (data_length % block_element_count == 0 ? 0 : 1);
}

int CalculateBlockSize(int data_length, int type_lenght, int block_element_count, bool last_block)
{
	return last_block ? 
		type_lenght*block_element_count : 
		(data_length - ((CalculateBlockCount(data_length, block_element_count) - 1) * block_element_count))*type_lenght;
}

//CompressedColumn* GPU_CompressColumn_INT(int block_element_count, int* ints, int data_length, CompressionChain* compression_chain)
//{
//	CompressedColumn *compressed_column = new CompressedColumn();
//	int type_lenght = sizeof(int);
//	int block_count = CalculateBlockCount(data_length, block_element_count);
//	
//	compressed_column->block_count = block_count;
//	compressed_column->data_blocks = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock)*block_count);
//	compressed_column->Type = INT;
//
//	auto current_chain = compression_chain;
//	while (current_chain != nullptr)
//	{
//		CompressionChain *comp_chain = (CompressionChain*)malloc(sizeof(CompressionChain));
//		comp_chain->Type = current_chain->Type;
//		comp_chain->next = compressed_column->compression_chain;
//		compressed_column->compression_chain = comp_chain;
//		
//		int processed_bytes = 0;
//		for (int i = 0; i < block_count; i++)
//		{
//			int block_size = CalculateBlockSize(data_length, type_lenght, block_element_count, (i < block_count - 1));
//			int *block = (int*)malloc(block_size);
//			memcpy_s(block, block_size, ints + processed_bytes, block_size);
//
//			compressed_column->data_blocks[i] = RLE_Compress(block, block_size / sizeof(int));
//			compressed_column->data_blocks[i] = NS_Compress(block, block_size / sizeof(int));
//			processed_bytes += block_size;
//		}
//
//		current_chain = current_chain->next;
//	}
//
//	return compressed_column;
//}

CompressedColumnBlock* GPU_CompressColumnBlock(CompressedColumnBlock* column_block, DataType type, CompressionChain* compression_chain)
{
	auto current_chain = compression_chain;
	CompressedColumnBlock* compressed_block = column_block;
	while (current_chain != nullptr)
	{
		switch (current_chain->Type)
		{
		case RLE: compressed_block = &(RLE_Compress(*compressed_block)); break;
		case NS:compressed_block = &(NS_Compress(*compressed_block)); break;
		case DICT:compressed_block = &(DICT_Compress(*compressed_block)); break;
		case SCALE:compressed_block = &(SCALE_Compress(*compressed_block)); break;
		default: break;
		}

		current_chain = current_chain->next;
	}
	return compressed_block;
}


CompressedColumn* GPU_CompressColumn(Column column, int block_element_count)
{
	CompressedColumn *compressed_column = new CompressedColumn();
	int block_count = CalculateBlockCount(column.DataLength, block_element_count);
	int type_lenght = 0;
	char* startPointer = nullptr;

	switch (column.DataType)
	{
	case INT:
		type_lenght = sizeof(int);
		startPointer = (char*)column.Ints;
		break;
	case FLOAT:
		type_lenght = sizeof(float);
		startPointer = (char*)column.Floats;
		break;
	case STRING:
		type_lenght = sizeof(char);
		startPointer = (char*)column.Strings;
		break;
	default: break;
	}

	compressed_column->block_count = block_count;
	compressed_column->data_blocks = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock)*block_count);
	compressed_column->Type = column.DataType;
	compressed_column->Name = column.Name;
	compressed_column->NameLength = column.NameLength;


	auto current_chain = column.compression_chain;
	CompressionChain *comp_chain = nullptr;

	while (current_chain != nullptr)
	{
		CompressionChain *chain = (CompressionChain*)malloc(sizeof(CompressionChain));

		chain->Type = current_chain->Type;
		chain->next = comp_chain;
		comp_chain = chain;

		current_chain = current_chain->next;
	}

	compressed_column->compression_chain = comp_chain;

	int processed_bytes = 0;
	for (int i = 0; i < block_count; i++)
	{
		CompressedColumnBlock* column_block = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock));

		int block_size = CalculateBlockSize(column.DataLength, type_lenght, block_element_count, (i < block_count - 1));
		char *block = (char*)malloc(block_size);
		memcpy_s(block, block_size, startPointer + processed_bytes, block_size);
		column_block->Data = block;
		column_block->DataLenght = block_size;

		auto data_block = GPU_CompressColumnBlock(column_block, column.DataType, column.compression_chain);
		compressed_column->data_blocks[i] = *data_block;
		processed_bytes += block_size;
	}

	return compressed_column;
}


//CompressedColumn* GPU_CompressColumn_FLOAT(int block_element_count, float* floats, int data_length)
//{
//	CompressedColumn *compressed_column = new CompressedColumn();
//	int type_lenght = sizeof(float);
//	int block_count = CalculateBlockCount(data_length, block_element_count);
//
//	compressed_column->block_count = block_count;
//	compressed_column->data_blocks = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock)*block_count);
//	compressed_column->Type = FLOAT;
//
//	CompressionChain *compression_chain = (CompressionChain*)malloc(sizeof(CompressionChain));
//	compression_chain->Type = SCALE;
//	compression_chain->next = compressed_column->compression_chain;
//	compressed_column->compression_chain = compression_chain;
//
//	int processed_bytes = 0;
//	for (int i = 0; i < block_count; i++)
//	{
//		int block_size = CalculateBlockSize(data_length, type_lenght, block_element_count, (i < block_count - 1));
//		float *block = (float*)malloc(block_size);
//		memcpy_s(block, block_size, floats + processed_bytes, block_size);
//
//		auto scaled = SCALE_Compress(block, block_size / sizeof(float));
//
//		compressed_column->data_blocks[i] = NS_Compress((int*)scaled.Data, scaled.DataLenght / sizeof(int));
//		processed_bytes += block_size;
//	}
//	return compressed_column;
//}

CompressedColumn* GPU_CompressColumn_STRING(int block_element_count, char** strings, int data_length, int string_lenght)
{
	CompressedColumn *compressed_column = new CompressedColumn();
	int type_lenght = sizeof(int)*string_lenght;
	int block_count = CalculateBlockCount(data_length, block_element_count);

	compressed_column->block_count = block_count;
	compressed_column->data_blocks = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock)*block_count);
	compressed_column->Type = STRING;

	CompressionChain *compression_chain = (CompressionChain*)malloc(sizeof(CompressionChain));
	compression_chain->Type = DICT;
	compression_chain->next = compressed_column->compression_chain;
	compressed_column->compression_chain = compression_chain;

	int processed_bytes = 0;
	for (int i = 0; i < block_count; i++)
	{
		int block_size = CalculateBlockSize(data_length, type_lenght, block_element_count, (i < block_count - 1));
		auto *block = (char*)malloc(block_size);
		for (int j = 0; j < block_element_count && j < data_length; j++)
		{
			for (int k = 0; k < string_lenght; k++)
			{
				block[j*string_lenght + k] = strings[j][k];
			}
		}

		CompressedColumnBlock compressed_column_block;

		compressed_column_block.Data = block;
		compressed_column_block.DataLenght = block_size;
		compressed_column->data_blocks[i] = compressed_column_block;

		processed_bytes += block_size;
	}
	return compressed_column;
}

int CalculateColumnDataLength(Column column)
{
	int lenght = 0;
	switch (column.DataType)
	{
	case INT: lenght = column.DataLength * sizeof(int); break;
	case FLOAT: lenght = column.DataLength * sizeof(float); break;
	case STRING: lenght = column.DataLength * column.StringLengths * sizeof(char); break;
	default: break;
	}
	return lenght;
}

int CalculateBlockElementCount(Table table)
{
	Column maxLenghtColumn = table.Columns[0];
	for (int i = 1; i < table.ColumnCount;i++)
	{
		if (CalculateColumnDataLength(table.Columns[i])>CalculateColumnDataLength(maxLenghtColumn))
			maxLenghtColumn = table.Columns[i];
	}

	int typesize = 0;
	switch (maxLenghtColumn.DataType)
	{
	case INT: typesize = sizeof(int); break;
	case FLOAT: typesize = sizeof(float); break;
	case STRING: typesize = sizeof(char)*maxLenghtColumn.StringLengths; break;
	default: break;
	}

	int chunk_size = CalculateChunkSize(typesize);

	return chunk_size / typesize;
}