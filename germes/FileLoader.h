#ifndef FileLoader_H
#define FileLoader_H

#include "TimeInMs.h"
#include "Paths.h"
#include <string>
#include "Table.h"
#include <vector>

#define METAFILE_EXT ".GPUDB_META"
#define COLUMN_DATA_EXT ".GPUDB_COLUMN"
#define GPUDB_EXT ".GPUDB"
#define COLUMN_NAME_SIZE 50

namespace File
{
	struct ColumnMetaInformation
	{
		char* Name;
		int NameLengt;
		int Size = 0;
		germes::CompressionChain* compression_chain;
		germes::DataType Type;
	};

	struct TableMetaInformation
	{
		char Name[COLUMN_NAME_SIZE];
		ColumnMetaInformation *Columns;
	};

	struct DatabaseMetaInformation
	{
		char Name[COLUMN_NAME_SIZE];
		TableMetaInformation *Tables;
	};

	struct ColumnFileData
	{
		germes::DataType Type;
		std::vector < int > Ints;
		std::vector < float > Floats;
		std::vector < char* > Strings;
	};


	class FileLoader
	{
	public:
		FileLoader::FileLoader()
		{
			_paths = new File::Paths("storage", "test");
		}

		FileLoader::~FileLoader()
		{
		}

		germes::Table* LoadUncompressedTable(std::string tableName, std::string path);
		germes::CompressedDatabase FileLoader::LoadCompressed();
		void SaveCompressedDb(germes::CompressedDatabase db);
		void SaveCompressedTable(germes::CompressedTable table);
	private:
		std::vector<ColumnMetaInformation> LoadMetas(std::string cs);
		void SaveCompressedColumn(std::string table_directory, germes::CompressedColumn column);
		void WriteCompressionChain(std::ofstream* out_file, germes::CompressionChain* chain);
		germes::CompressionChain* LoadCompressionChain(std::ifstream* in_file);
		void SaveCompressedColumnBlock(std::string table_name, std::string col_name, germes::CompressedColumnBlock column, int bloc_number);
		void SaveCompressedTableMeta(germes::CompressedTable table);
		void ParseLine(char* line, int length, char delimiter, std::vector<ColumnFileData>* columns);
		void LoadCompressedTable(germes::CompressedTable* table);
		void LoadCompressedColumn(std::string table_name, germes::CompressedColumn* column);
		void LoadCompressedColumnBlock(std::string table_name, std::string col_name, germes::CompressedColumnBlock* column, int block_number);
		int MaxStringLenght(std::vector < char* >);
		char *FillString(char *str, int size);
		File::Paths* _paths;
	};
}

namespace boost {
	namespace serialization {

		template<class Archive>
		void serialize(Archive & ar, File::ColumnMetaInformation & g, const unsigned int version)
		{
			ar & g.Name;
			ar & g.CompressionType;
			ar & g.Size;
			ar & g.Type;
		}

		template<class Archive>
		void serialize(Archive & ar, germes::CompressedColumnBlock & g, const unsigned int version)
		{
			ar & g.Type;
			ar & g.DataLenght;

			for (int i = 0; i < g.DataLenght; i++)
			{
				if (g.Data!=nullptr)
					ar & g.Data[i];
				if (g.Custom1 != nullptr)
					ar & g.Custom1[i];
				if (g.Custom2 != nullptr)
					ar & g.Custom2[i];
				if (g.Custom3 != nullptr)
					ar & g.Custom3[i];
				if (g.Custom4 != nullptr)
					ar & g.Custom4[i];
			}
		}

	} // namespace serialization
} // namespace boost

#endif
