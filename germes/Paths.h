#ifndef Paths_H
#define Paths_H

#include <string>

namespace File
{
	class Paths
	{
	public:
		File::Paths::Paths(std::string base_dir, std::string database_name)
		{
			BaseDir = base_dir;
			DatabaseName = database_name;
			DatabaseDir = BaseDir + "\\" + DatabaseName;
		}

		File::Paths::~Paths()
		{ }

		std::string BaseDir;
		std::string DatabaseName;
		std::string DatabaseDir;
		std::string GetTableDir(char *name);
		std::string GetColumnDir(char *table_name, char* column_name);
		std::string GetColumnBlockPath(char *table_name, char* column_name, int block_number);
	private:

	};
}

#endif
