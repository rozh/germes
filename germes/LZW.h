void lzwCompress(char* input, int input_length, short** output, int* output_length);
void lzwDecompress(short* input, int input_length, char** output, int* output_length);