#include "gcompress_cuda.h"
#include "Table.h"

/**
��������� ������� � GPU � ��������� ��

@compressed_table		������ �������
@column_block_number	����� �����
@return Table �� �������� �� GPU
*/
germes::Table GPU_LoadTableBlock(germes::CompressedTable compressed_table, int column_block_number);

/**
��������� ������� � GPU � ��������� ��

@table		�������� �������
@gpuTable	������� � GPU
@return Table �� �������� �� GPU
*/
germes::Table GPU_RetriveTable(germes::Table table, germes::Table gpuTable);

/**
��������� ������� � GPU � ������������ ���

@table		�������� �������
@return CompressedTable ������ �������
*/
germes::CompressedTable* GPU_CompressTable(germes::Table table);

/**

*/
int* GPU_CopyToDevice(int *data, int len);

/**

*/
void GPU_Free(int *data);

int CalculateBlockElementCount(germes::Table table);
germes::CompressedColumn* GPU_CompressColumn(germes::Column col, int block_element_count);


germes::CompressedColumnBlock* GPU_CompressColumnBlock_INT(int *data, int len);