#include <stdio.h>
#include "GPU_proccess.h"
#include "compress_cpu.h"
#include <cstdlib>
#include "FileLoader.h"
#include <iostream>
#include <fstream>
#include "LZW.h"

#define element_count (1024*1024*150)
#define compression_ratio 10

using namespace germes;

void test(int elementCount, int compressionRatio)
{
	std::ofstream of;
	of.open("log.txt", std::ios::out | std::ios::app);

	int *data = (int*)malloc(sizeof(int) * elementCount / compressionRatio);
	int *len = (int*)malloc(sizeof(int) * elementCount / compressionRatio);
	for (int i = 0; i < elementCount / compressionRatio; i++)
	{
		data[i] = i;
		len[i] = compressionRatio;
	}

	CompressedColumnBlock *column_block = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock) * 2);
	column_block[0] = *new CompressedColumnBlock();
	column_block[1] = *new CompressedColumnBlock();
	column_block[0].Data = (char*)data;
	column_block[0].DataLenght = (elementCount / compressionRatio)* sizeof(int);
	column_block[0].Custom1 = (char *)len;
	column_block[1].Data = (char*)data;
	column_block[1].DataLenght = (elementCount / compressionRatio)* sizeof(int);
	column_block[1].Custom1 = (char *)len;

	CompressedColumn *ccol = (CompressedColumn *)malloc(sizeof(CompressedColumn));;
	ccol[0].Name = "id";
	ccol[0].NameLength = 2;
	ccol[0].block_count = 2;
	ccol[0].data_blocks = column_block;
	ccol[0].Type = germes::INT;

	CompressedTable ctab;
	ctab.Name = "table";
	ctab.NameLength = 7;
	ctab.column_count = 1;
	ctab.compressed_columns = ccol;
	
	uint64 start_time = GetTimeMs64();
	auto gpu_tab = GPU_LoadTableBlock(ctab, 0);
	of << "decompress x" << compressionRatio << '\t' << elementCount << '\t' << GetTimeMs64() - start_time << std::endl;
	GPU_Free(gpu_tab.Columns[0].Ints);
	
	int *data2 = (int*)malloc(sizeof(int) * elementCount);
	for (int i = 0; i < elementCount; i++)
	{
		data2[i] = i;
	}

	start_time = GetTimeMs64();

	int *gpu = GPU_CopyToDevice(data2, elementCount);

	of << "copy only x" << compressionRatio << '\t' << elementCount << '\t' << GetTimeMs64() - start_time << std::endl;
	GPU_Free(gpu);

	of.close();
	free(data);
	free(data2);
	free(len);
	free(column_block);
	free(ccol);
}

void test_cpu(int elementCount, int compressionRatio)
{
	std::ofstream of;
	of.open("log_cpu.txt", std::ios::out | std::ios::app);

	int *data = (int*)malloc(sizeof(int) * elementCount / compressionRatio);
	int *len = (int*)malloc(sizeof(int) * elementCount / compressionRatio);
	for (int i = 0; i < elementCount / compressionRatio; i++)
	{
		data[i] = i;
		len[i] = compressionRatio;
	}

	int *vals;
	int lenght;

	uint64 start_time = GetTimeMs64();

	RLE_decompress(&vals, &lenght, data, len, elementCount / compressionRatio);
	of << "decompress x" << compressionRatio << '\t' << elementCount << '\t' << GetTimeMs64() - start_time << std::endl;

	of.close();
	free(data);
	free(len);
	free(vals);
}

void benchmark()
{
	test(1000, 2);
	for (int i = 100; i < 22100; i+=100)
	{
		for (int j = 2; j <= 10; j++)
			test_cpu(1000 * i, j);
	}
}

void test_lzw()
{
	char* text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac felis non tellus auctor pulvinar ut eu justo. Mauris ac diam turpis. Phasellus et mi in lacus hendrerit tincidunt a ultricies urna. Quisque iaculis libero nec nisl vehicula viverra. Integer porttitor dui a nisi fermentum consequat. Mauris leo turpis, semper at interdum id, dignissim eget nisl. Maecenas mattis non massa sit amet commodo. Praesent maximus quam vel tellus porta fermentum. In hac habitasse platea dictumst. Suspendisse fringilla egestas tincidunt. Fusce tristique tempus quam nec eleifend. Praesent venenatis dolor rutrum, iaculis enim vitae, imperdiet neque. Sed convallis, lacus vitae imperdiet dictum, nulla elit porttitor risus, ac tincidunt magna arcu at justo. Aenean iaculis pharetra velit vel lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus.";
	short* compress = nullptr;
	int* compress_length = (int*)malloc(sizeof(int));
	lzwCompress(text, 853, &compress, compress_length);

	char * decompress = nullptr;
	int* decompress_length = (int*)malloc(sizeof(int));
	lzwDecompress(compress, *compress_length, &decompress, decompress_length);

}

int main()
{
	test_lzw();
	//benchmark();

	//int *data = (int*)malloc(sizeof(int) * element_count / compression_ratio);
	//int *len = (int*)malloc(sizeof(int) * element_count / compression_ratio);
	//for (int i = 0; i < element_count / compression_ratio; i++)
	//{
	//	data[i] = i;
	//	len[i] = compression_ratio;
	//}
	//
	//CompressedColumnBlock *column_block = (CompressedColumnBlock *)malloc(sizeof(CompressedColumnBlock)*2);
	//column_block[0] = *new CompressedColumnBlock();
	//column_block[1] = *new CompressedColumnBlock();
	//column_block[0].Type = RLE;
	//column_block[0].Data = (char*)data;
	//column_block[0].DataLenght = (element_count / compression_ratio)* sizeof(int);
	//column_block[0].Custom1 = (char *)len;
	//column_block[1].Type = RLE;
	//column_block[1].Data = (char*)data;
	//column_block[1].DataLenght = (element_count / compression_ratio)* sizeof(int);
	//column_block[1].Custom1 = (char *)len;
	//
	//CompressedColumn *ccol = (CompressedColumn *)malloc(sizeof(CompressedColumn));;
	//ccol[0].Name = "id";
	//ccol[0].NameLength = 2;
	//ccol[0].block_count = 2;
	//ccol[0].data_blocks = column_block;
	//
	//CompressedTable ctab;
	//ctab.Name = "table";
	//ctab.NameLength = 7;
	//ctab.column_count = 1;
	//ctab.compressed_columns = ccol;
	//
	//
	//uint64 start_time = GetTimeMs64();
	//auto gpu_tab = GPU_LoadTableBlock(ctab, 0);
	//std::cout << GetTimeMs64() - start_time << " ms" << std::endl;
	//GPU_Free(gpu_tab.Columns[0].Ints);
	//
	//start_time = GetTimeMs64();
	//gpu_tab = GPU_LoadTableBlock(ctab, 0);
	//std::cout << GetTimeMs64() - start_time << " ms" << std::endl;
	//GPU_Free(gpu_tab.Columns[0].Ints);

	//Table tab = {};

	//auto host_tab = GPU_RetriveTable(tab, gpu_tab);

	//gpu_tab = GPU_LoadTableBlock(ctab, 0);
	//host_tab = GPU_RetriveTable(host_tab, gpu_tab);
	//gpu_tab = GPU_LoadTableBlock(ctab, 0);
	//host_tab = GPU_RetriveTable(host_tab, gpu_tab);
	//gpu_tab = GPU_LoadTableBlock(ctab, 0);
	//host_tab = GPU_RetriveTable(host_tab, gpu_tab);

	//for (int i = 0; i < element_count*2; i++)
	//{
	//	printf("%d ", host_tab.Columns[0].Ints[i]);
	//}


	File::FileLoader* test = new File::FileLoader();
	Table* table = test->LoadUncompressedTable("lineitem", "C:\\Users\\Roman\\Documents\\Visual Studio 2013\\Projects\\gpu_compress\\gpu_compress.test\\lineitem2.tbl");

	auto cTable = GPU_CompressTable(*table);
	File::FileLoader* fl = new File::FileLoader();

	CompressedDatabase cdb;
	cdb.Name = "test2";
	cdb.NameLength = 5;
	cdb.compressed_tables = cTable;
	cdb.table_count = 1;

	fl->SaveCompressedDb(cdb);

	auto db = fl->LoadCompressed();
	uint64 start_time = GetTimeMs64();
	
	Table *gpu_tab = (Table*)malloc(db.compressed_tables[0].compressed_columns[0].block_count*sizeof(Table));
	
	for (int i = 0; i < db.compressed_tables[0].compressed_columns[0].block_count; i++)
	{
		gpu_tab[i] = GPU_LoadTableBlock(db.compressed_tables[0], i);
	}
	
	std::cout << GetTimeMs64() - start_time << " ms" << std::endl;
	
	start_time = GetTimeMs64();
	
	Table *gpu_tab2 = (Table*)malloc(db.compressed_tables[0].compressed_columns[0].block_count*sizeof(Table));
	
	for (int i = 0; i < db.compressed_tables[0].compressed_columns[0].block_count; i++)
	{
		gpu_tab2[i] = GPU_LoadTableBlock(db.compressed_tables[0], i);
	}
	for (int i = 0; i < db.compressed_tables[0].compressed_columns[0].block_count; i++)
	{
		gpu_tab2[i] = GPU_LoadTableBlock(db.compressed_tables[0], i);
	}
	for (int i = 0; i < db.compressed_tables[0].compressed_columns[0].block_count; i++)
	{
		gpu_tab2[i] = GPU_LoadTableBlock(db.compressed_tables[0], i);
	}
	for (int i = 0; i < db.compressed_tables[0].compressed_columns[0].block_count; i++)
	{
		gpu_tab2[i] = GPU_LoadTableBlock(db.compressed_tables[0], i);
	}
	
	std::cout << GetTimeMs64() - start_time << " ms" << std::endl;

	//int *data2 = (int*)malloc(sizeof(int) * element_count);
	//for (int i = 0; i < element_count; i++)
	//{
	//	data2[i] = i;
	//}
	//
	//start_time = GetTimeMs64();
	//
	//int *gpu = GPU_CopyToDevice(data2, element_count);
	//
	//std::cout << GetTimeMs64() - start_time << " ms" << std::endl;
	//GPU_Free(gpu);

	//int *values = (int*)db.compressed_tables[0].compressed_columns[0].data_blocks[0].Data;
	//int *sizes = (int*)db.compressed_tables[0].compressed_columns[0].data_blocks[0].Custom1;
 //
	//Table tab2 = {};
	//auto gpu_tab2 = GPU_LoadTableBlock(db.compressed_tables[0], 0);
	//auto host_tab2 = GPU_RetriveTable(tab2, gpu_tab2);


    return 0;
}


