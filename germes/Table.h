#ifndef Table_H
#define Table_H


#define CHUNK_SIZE (20*1024) // 20 ��

namespace germes
{
	struct CompressionChain;

	enum DataType
	{
		INT = 1,
		FLOAT = 2,
		STRING = 3,
	};

	enum CompressionType
	{
		RLE = 1,
		NS = 2,
		DICT = 3,
		SCALE = 4,
	};


	// ��������� ��� ������������� ������� ������
	struct Column
	{
		char *Name;					//��� �������
		int NameLength;				//����� ����� �������
		int DataLength;				//����� ����� ������
		DataType DataType;				//��� ������:
		// 0 - INT
		// 1 - FLOAT
		// 2 - STRING
		int *Ints;					//����
		float *Floats;				//������
		char **Strings;				//�������
		int StringLengths;			//����� ��������
		CompressionChain *compression_chain;	//������� ������
	};

	// ��������� ������������� �������
	struct Table
	{
		char *Name;					//��� �������
		int NameLength;				//����� ����� �������
		Column *Columns;			//������� �������
		int ColumnCount;			//���������� �������� � �������
	};

	// ��������� ������������� ��
	struct Database
	{
		char *Name;					//��� ��
		int NameLength;				//����� ����� ��
		Table *Tables;				//�������
		int TableCount;				//���������� ������
	};

	// ��������� ������� ����� �������
	struct CompressedColumnBlock
	{
		CompressedColumnBlock() : Data(nullptr),
			DataLenght(0)
		{
			Custom1 = nullptr;
			Custom2 = nullptr;
			Custom3 = nullptr;
			Custom4 = nullptr;
		}

		char *Data;					//������
		int DataLenght;				//����� ������
		char *Custom1;				//������������ ���� 1
		char *Custom2;				//������������ ���� 2
		char *Custom3;				//������������ ���� 3
		char *Custom4;				//������������ ���� 4
	};

	struct CompressionChain
	{
		CompressionChain(): Type(RLE), next(nullptr)
		{ }
		CompressionType Type;	//��� ������
		CompressionChain *next;	//��������� �������� ������
	};

	//������ �������
	struct CompressedColumn
	{
		CompressedColumn() : Name(nullptr), NameLength(0), data_blocks(nullptr), block_count(0), Type(INT), compression_chain(nullptr)
		{ }

		char *Name;								//��� �������
		int NameLength;							//����� ����� �������
		CompressedColumnBlock *data_blocks;		//����� ������
		int block_count;						//���������� ������ ������
		DataType Type;							//��� ������ ������
		CompressionChain *compression_chain;	//������� ������
	};

	//������ �������
	struct CompressedTable
	{
		CompressedTable() : Name(""),
			NameLength(0), compressed_columns(nullptr), column_count(0)
		{}

		char *Name;								//��� �������
		int NameLength;							//����� ����� 
		CompressedColumn *compressed_columns;	//������
		int column_count;						//���������� �������
	};

	//������ ��
	struct CompressedDatabase
	{
		char *Name;								//��� ��
		int NameLength;							//����� ����� 
		CompressedTable *compressed_tables;		//�������
		int table_count;						//���������� ������
	};
}

#endif
